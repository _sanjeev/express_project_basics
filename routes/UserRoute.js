const express = require("express");
const router = express.Router();

const { userLogin } = require("./../controller/UserController");

router.use(express.json());

router.post("/login", userLogin);

module.exports = router;
