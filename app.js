const express = require("express");
const app = express();

app.use("/", require("./routes"));

app.listen(5000, (err) => {
    if (err) {
        console.log("Error : ", err);
        return;
    }
    console.log("Server is listening on port 5000");
});
